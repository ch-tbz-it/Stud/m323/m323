# Aufgaben: Aus Anforderungen Funktionen ableiten


## Aufgabe 1 - Beispiele umsetzen als Funktionen

Verwenden Sie Ihre Skizzen zu den beiden Anforderungen aus den Unterlagen und setzen Sie diese im Code um. Sie können Java oder Scala verwenden.

Wichtig: der Code muss die Funktionen abbilden.

Hier nochmals die Aufgabenstellung: **"Eine Reise planan"**

Wir planen eine Reise durch Europa und möchten, dass der Benutzer 
jeweils die Destinationen für die Reise eingibt.
Zudem soll es möglich sein, eine bereits festgelegte Route zu ändern 
(z.Bsp. wenn Ihr Freund noch einen anderen Zwischenaufenthalt möchte).



## Aufgabe 2 - "Wörter mit Punkten bewerten"

Wir wollen eine App, die dem User erlaubt, Wörter einzugeben. Jedes Wort erhält für jeden Buchstaben einen Punkt, solange der Buchstabe nicht "a" ist.
Zudem wollen wir eine Liste ausgeben, sortiert nach den Wörtern mit der höchsten Punktzahl.


## Aufgabe 3 - "Autorennen"

Wir möchten eine App, welche für ein Auto-Rennen die gesamte Zeit für 
alle Runden berechnet. Die App soll auch die Durchschnittszeit pro 
Auto berechnen. Die erste Runde wird nicht mitgezählt, da es sich 
hier um eine "Warm-up" Runde handelt.

## Verwenden von Scala

Falls Sie Scala verwenden, schauen Sie sich dieses Tutorial zu Functional Programming an:
https://www.youtube.com/watch?v=74Ia6lvVMyc

